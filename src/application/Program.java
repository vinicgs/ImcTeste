package application;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import entities.Dados;
import entities.Imprimir;

public class Program {

	public static void main(String[] args) {

		String path = "src\\resources\\dataset.CSV";

		List<Dados> lista = new ArrayList<>();

		try (BufferedReader br = new BufferedReader(new FileReader(path))) {

			String line = br.readLine();

			while ((line = br.readLine()) != null) {

				String[] vetor = line.split(";");

				Dados dados = new Dados();

				if (vetor.length == 4) {
					dados.setNome(vetor[0], vetor[1]);
					dados.setImc(vetor[2], vetor[3]);
				} else {
					dados.setNome(vetor[0], vetor[1]);
					dados.setImc("0", "0");
				}
				lista.add(dados);
			}

			for (Dados dados : lista) {
				System.out.println(dados);
			}

			Imprimir.imprimir(lista);

		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}
